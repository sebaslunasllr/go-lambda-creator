#/bin/sh
echo "Building..."

file=".golambda"
package=false
while getopts "p" o; do
    case "${o}" in
        p) 
          package=true
          ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
done

function prop {
    grep "${1}" $file|cut -d'=' -f2
}

if [ ! -f "$file" ]; then
    echo -e "\e[31m$file not found :'("
    exit 1
fi

project="$(prop 'project.name')"
if [ "$project" == "" ]; then
    echo "No project.name found in $file"
    exit 1
fi

env GOOS=linux go build -ldflags="-s -w" -o bin/$project cmd/$project.go

if [ "$package" == true ] ; then
    echo "Packaging..."
    unameOut="$(uname -s)"
    case "${unameOut}" in
        Linux*)     zip bin/$project.zip bin/$project ;;
        Darwin*)    zip bin/$project.zip bin/$project ;;
        CYGWIN*)    build-lambda-zip.exe -output bin/$project.zip bin/$project ;;
        MINGW*)     build-lambda-zip.exe -output bin/$project.zip bin/$project ;;
        *)          machine="UNKNOWN:${unameOut}"
    esac
fi