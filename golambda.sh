#!/bin/bash
create_lambda=false
build_lambda=false
package_lambda=false
start_lambda=false
name="go_lambda"
while getopts "cbspn:" o; do
    case "${o}" in
        n) 
          name="$OPTARG"
          ;;
        c) create_lambda=true
          ;;
        b) 
          build_lambda=true
          ;;
        p)
          build_lambda=true
          package_lambda="-p"
          ;;
        s)
          start_lambda=true
          ;;
        \?) echo "Invalid option: -$OPTARG" >&2; exit 1 ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
done

if [ "$create_lambda" == true ] ; then
  sh create.sh -n $name
fi

if [ "$build_lambda" == true ] ; then
  sh build.sh "$package_lambda"
fi

if [ "$start_lambda" == true ] ; then
  sam local start-api
fi
