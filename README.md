# Lambda Go Creator

To install, add the lambda-creator.cmd to the file path.
Use the git console to run this project.

Boilerplate to build projects in go using lambda functions.
To start the project locally write in the console: `sam local start-api`

## How to use

You can place the lambda-creator.cmd file in the folder that you want to create the lambda, open a cmd terminal (git terminal recommended) and then just calling the command `golambda.sh -c -n name`.
The script will ask you for the lambda name and that's it!

You can also add the lambda-creator.cmd to the path.
