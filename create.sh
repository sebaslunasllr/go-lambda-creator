#!/bin/bash
echo Initilizing lambda creation
name=go_lambda
while getopts "n:" o; do
    case "${o}" in
        n) 
          name="$OPTARG"
          ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
done

go get -u github.com/aws/aws-lambda-go/cmd/build-lambda-zip
mkdir "$name"
cd $name
echo project.name=$name>> .golambda

echo "# $name initialized"> README.md
echo -e "">> README.md
echo "Project initilized using [go-lambda-creator](https://gitlab.com/sebaslunasllr/go-lambda-creator/-/tree/master). ">> README.md
echo "To start the project locally write in the console: \`golambda.sh -b -s\`.>">> README.md
echo "To build the project use \`golambda.sh -b\`">> README.md
echo "For every change you will need to rebuild the project using the command stated above.">> README.md
echo -e "">> README.md
echo "## Before running">> README.md
echo -e "">> README.md
echo "### Requisites">> README.md
echo -e "">> README.md
echo "[SAM](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install-windows.html).">> README.md
echo "[Docker](https://www.docker.com/).">> README.md

mkdir cmd
cd cmd 

echo "package main">> $name.go
echo "import (">> $name.go
echo "  \"github.com/aws/aws-lambda-go/events\"">> $name.go
echo "  \"github.com/aws/aws-lambda-go/lambda\"">> $name.go
echo ")">> $name.go
echo -e "">> $name.go
echo "type response struct {">> $name.go
echo "  Message string \`json:\"message\"\`">> $name.go
echo "}">> $name.go
echo -e "">> $name.go
echo "func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {">> $name.go
echo "  return events.APIGatewayProxyResponse{">> $name.go
echo "    Body:       \"Go Serverless v1.0! Your function executed successfully!\"," >> $name.go
echo "    StatusCode: 200,">> $name.go
echo "   }, nil">> $name.go
echo "}">> $name.go
echo -e "">> $name.go
echo "func main() {">> $name.go
echo "  lambda.Start(handler)">> $name.go
echo "}">> $name.go

cd ..
mkdir pkg
cd pkg
mkdir main
cd main
mkdir handlers
cd handlers
echo "package handlers"> handlers.go
cd ../..
mkdir test
cd test
mkdir handlers
cd handlers
echo "package handlers"> handlers_test.go
echo "//import \"testing\"">> handlers_test.go
cd ../../..

echo "AWSTemplateFormatVersion : '2010-09-09'"> template.yml
echo "Transform: AWS::Serverless-2016-10-31">> template.yml
echo "Description: Lambda $name application.">> template.yml
echo "Resources:">> template.yml
echo "  HelloWorldFunction:">> template.yml
echo "    Type: AWS::Serverless::Function">> template.yml
echo "    Properties:">> template.yml
echo "      Handler: bin/$name">> template.yml
echo "      Runtime: go1.x">> template.yml
echo "      Events:">> template.yml
echo "        Vote:">> template.yml
echo "          Type: Api">> template.yml
echo "          Properties:">> template.yml
echo "            Path: /">> template.yml
echo "            Method: get">> template.yml
echo "  HelloWorldFunction2:">> template.yml
echo "    Type: AWS::Serverless::Function">> template.yml
echo "    Properties:">> template.yml
echo "      Handler: bin/$name">> template.yml
echo "      Runtime: go1.x">> template.yml
echo "      Events:">> template.yml
echo "        Vote:">> template.yml
echo "          Type: Api">> template.yml
echo "          Properties:">> template.yml
echo "            Path: /">> template.yml
echo "            Method: post">> template.yml

echo "# Serverless directories">> .gitignore
echo ".serverless">> .gitignore
echo -e "">> .gitignore
echo "# golang output binary directory">> .gitignore
echo "bin">> .gitignore
echo -e "">> .gitignore
echo "# golang vendor (dependencies) directory">> .gitignore
echo "vendor">> .gitignore
echo -e "">> .gitignore
echo "# Binaries for programs and plugins">> .gitignore
echo "*.exe">> .gitignore
echo "*.exe~">> .gitignore
echo "*.dll">> .gitignore
echo "*.so">> .gitignore
echo "*.dylib">> .gitignore
echo -e "">> .gitignore
echo "# Test binary, build with `go test -c`">> .gitignore
echo "*.test">> .gitignore
echo -e "">> .gitignore
echo "# Output of the go coverage tool, specifically when used with LiteIDE">> .gitignore
echo "*.out">> .gitignore

go mod init $name
echo -e "" > go.sum

go get -v all
env GOOS=linux go build -ldflags="-s -w" -o bin/$name cmd/$name.go
echo -e ""
echo -e "\e[92mCreated:"
echo -e "\e[92m* \e[97mGet into the folder \e[92mcd $name"
echo -e "\e[92m* \e[97mUse \e[92mgolambda.sh -b -s \e[97mto start the application"
echo -e "\e[92m* \e[97mFor every change you'll need to rebuild you app using \e[92mgolambda.sh -b -s"
echo -e "\e[92m* \e[97mUser \e[92mgolambda.sh -p \e[92mto build zip to upload to AWS"